#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include "sbuf.h"
#include "csapp.h"
#define NTHREADS 4
#define SBUFSIZE 16

/*
	//ECHO CLIENT MAIN ROUTINE FROM THE SLIDES
	int clientfd, port;			//client file descriptor, port number
	char* host, buf[MAXLINE];
	rio_t rio;
	
	host = argv[1];
	port = atoi(argv[2]);
	
	clientfd = Open_clientfd(host, port);
	
    Rio_readinitb(&rio, clientfd); 			//initialized buffer for readlineb func. which reads in line by line
	
    printf("Enter message:");
	
	fflush(stdout);
	
    while (Fgets(buf, MAXLINE, stdin) != NULL) { 
        Rio_writen(clientfd, buf, strlen(buf)); 
        Rio_readlineb(&rio, buf, MAXLINE); 
		
        printf("Echo:");
        Fputs(buf, stdout); 
        printf("Enter message:"); fflush(stdout);
    } 
    Close(clientfd); 
*/

int main*int argc, char const *argv[]){
	int i, listenfd, connfd, port;
	socklen_t clientlen = sizeof(struct sockaddr_in);
	struct socaddr_in clientaddr;
	pthread_t tid;
	
	if(argc != 2){
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = atoi(argv[1]);
	sbuf_init(&sbuf, SBUFSIZE);
	listenfd = Open_listenfd(port);
	
	for(i = 0; i < NTHREADS; i++){
		Pthread_create(&tid, NULL, thread, NULL);
	}
	
	while(1){
		connfd = Accept(listenfd, (SA *) &clientaddr, &clientlen);
		sbuf_insert(&sbuf, connfd);
	}
}

void *thread(void *vargp){
	Pthread_detach(pthread_self());
	while(1){
		int connfd = sbuf_remove(&sbuf);
		echo_cnt(connfd);
		Close(connfd);
	}
}

void echo(int connfd){
	int clientfd, port;
	char* host, buf[MAXLINE];
	rio_t rio;
	host = argv[1]; port = atoi(argv[2]);
	clientfd = Open_clientfd(host,port);
	Rio_readinitb(&rio, clientfd);
	printf("Enter message:"); fflush(stdout);
	while(fgets(buf,MAXLINE,stdin)!=NULL){
		Rio_writen(clientfd,buf,strlen(buf));
		Rio_readlineb(&rio,buf,MAXLINE);
		printf("Echo:");
		fputs(buf,stdout);
		printf("Enter message:"); fflush(stdout);
	}
	Close(clientfd);
}