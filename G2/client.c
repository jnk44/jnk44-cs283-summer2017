#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "csapp.h"

/*
	************
	JOSHUA KARMEL
	8/18/17
	G2
	*************
	What works so far:
		Console input two prime numbers
		values of c, m, e, and d are calculated
*/

//PROCESS
//find the public key values, then use those values to encrypt/decrypt

//public key = (e,c)
//private key = (d,c)

/*
	a = first prime given
	b = second prime given
	c = a*b (the mod value used for encrypting/decrypting)
	m = totient of c (if a and b are primes, m = (a-1)* (b-1))
	e = ANY num, coprime to c & m, s.t. e % m != 1 (encryption key)
	d = e^[totient(m)-1] % m (decryption key)
	
	
*/

//totient(x) = size (i) of set: 1 to x s.t. gcd(i,x) == 1
// totient: how many GCDs == 1
//if x = P1P2, P1 and P2

//y = x^e % c
//x = y^d % c

/* 
	if(GCD(a) == 1 && GCD(b) ==1)
		e % m != 1;
*/

//Client	Server
/*
Loop{
	read(stdin)
	write(connfd)
}
Loop{
	read(connfd)
	write(stdout)
}
*/

//returns 0 if prime, 1 if not a prime
int checkPrime(int x){
	if(x <= 1){
		return 1;
	}
	if((x % 2 == 0) && (x > 2)){
		return 1;
	}
	
	int i;
	for(i = 3; i < (x/2); i += 2){
		if((x % i) == 0){
			return 1;
		}
	}
	return 0;
}


//calculate nth prime number
long nthPrime(int n){
	long i = 1;
	int primes = 0;
	
	while(primes < n){
		i++;
		if(checkPrime(i) == 0){
			primes++;
			//printf("i=%ld primes=%d\n",i,primes);
			
		}
		
	}
	
	return i;
}

//loop this to make sure result < z so that result does not overflow
long modPow(long x,long y,long z){
	long result = 1;
	
	x = x % z;
	
	while(y > 0){
		if(y & 1){
			result = (result * x) % z;
		}
		y >>= 1;
		x = (x*x)%z;
	}
	
	return result;
	//result x^y % z
}

//encrypts single character
//result = ch^e % c
char encrypt(char ch, int e, int c){
	//the encryption key
	return modPow(ch,e,c);
}

//encrypts a string, one character at a time, using encrypt
char* strencrypt(char* str, int e, int c){
	int i;
	
	char* result = (char*)malloc(strlen(str));
	
	for(i = 0; i < strlen(str); i++){
		result[i] = encrypt(str[i], e, c);
	}
	
	return result;
}

//decrypts a single character
//result = x^d % c
char decrypt(int ch, int d, int c){
	return modPow(ch,d,c);
}

//decrypts a string, once character at a time
char* strdecrypt(char* str, int d, int c){
	int i;
	
	char* result = (char*)malloc(strlen(str));
	
	for(i = 0; i < strlen(str); i++){
		result[i] = decrypt(str[i], d, c);
	}
	
	return result;
}

//returns the GCD of x and y
int gcd(int x, int y){
	int i;
	int result = 0;
	for(i = 1; i <= x && i <= y; i++){
		//printf("i %d\n",i);
		if((x % i == 0) && (y % i == 0)){
			result = i;
		}
	}
	return result;
}

int coprime(int x){
	int r = x*2;
	srand(time(NULL));
	while(1){
		r = rand();
		if(gcd(r,x) == 1){
			return r;
		}
	}
}

//Using a rand number generator, pick a random int that is coprime to the given input parameter x
long coprime2(int x, int y){
	long r = y*2;
	srand(time(NULL));
	while(1){
		r = rand()%32000;
		if(gcd(r,x) == 1 && gcd(r,y) == 1){
			return r;
		}
	}
	return r;
}

//size of set of numbers from 0-x s.t. gcd(x) == 1
int totient(int x){
	int result = 0;
	int i;
	
	for(i = 0; i <= x; i++){
		if(gcd(i,x) == 1){
			result++;
			//printf("tot: %d x: %d\n",result,x);
		}
	}
	
	return result;
}


int main(int argc, char** argv){
	
	int a = atoi(argv[1]);
	int b = atoi(argv[2]);
	
	long aprime = nthPrime(a);
	long bprime = nthPrime(b);

	printf("a prime = %ld\n",aprime);
	printf("b prime = %ld\n",bprime);
	
	long c = aprime * bprime;
	printf("c = %ld\n",c);
	
	long m = totient(c);
	printf("m = %ld\n",m);
	
	long e = coprime2(c,m);
	printf("e = %ld\n",e);
	
	long d = modPow(e,(totient(m)-1),m);
	printf("d = %ld\n\n",d);
	
	
	int v = 'b'-96;
	
	char enc = encrypt(v,e,c)+96;
	
	printf("encryption of %c(%d): %c(%d)\n",v+96,v,enc,enc);
	
	char dec = decrypt(enc,d,c);
	
	printf("decryption of %c(%d): %c(%d)\n",enc,enc,dec,dec);
	
	
	/*
	printf("'b' = %d\n",v);
	int x = pow(v,e);
	printf("'b'^e: %d\n",x);
	int z = x%c;
	printf("'b'^e mod c: %d\n",z);
	
	printf("modPow('b',e,c) = %d\n",modPow(v,e,c));
	
	printf("modpow: %d\n",modPow(2,5,13));
	*/
	
	//rio_writen(connf d, &val, sizeof(int));
	
	return 0;
	
}


