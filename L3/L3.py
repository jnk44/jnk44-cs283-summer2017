#!/usr/bin/env python
#	Joshua Karmel
#	8/24/17
#	Python 2.7
#	CS 283 - Lab 2

import csv
import sys

data = dict()
longestDelay = 0

with open('DelayedFlights.csv','rb') as csvfile:
	csvreader = csv.DictReader(csvfile)
	for row in csvreader:
		origin = row['Origin']
		carrier = row['UniqueCarrier']
		
		#creates dictionary for origins relating with weather/security delays and weather+security values
		if not row['Origin'] in data:
			data[origin] = dict()
			data[origin]['weather'] = []
			data[origin]['security'] = []
			data[origin]['w+s'] = []
			data[origin]['lad'] = [] #late aircraft delays
			
		#creates dict for carriers relating with carrier delays
		if not row['UniqueCarrier'] in data:
			data[carrier] = dict()
			data[carrier]['delay'] = []
			data[carrier]['lad'] = []
		
		#adds weather/security delays to respective lists
		if len(row['WeatherDelay']) > 1:
			weather = float(row['WeatherDelay'])
			data[origin]['weather'].append(weather)
		if len(row['SecurityDelay']) > 1:
			security = float(row['SecurityDelay'])
			data[origin]['security'].append(security)
		
		#adds the weather and security delays together and puts them in their own list for later addition
		if ((len(row['SecurityDelay']) > 1) and (len(row['WeatherDelay']) > 1)):
			ws = security + weather;
			data[origin]['w+s'].append(ws)
		
		#adds the carrier delays into respective list
		if len(row['CarrierDelay']) > 1:
			cdelay = float(row['CarrierDelay'])
			data[carrier]['delay'].append(cdelay)
			
		if len(row['LateAircraftDelay']) > 1:
			LACDelays = float(row['LateAircraftDelay'])
			data[origin]['lad'].append(LACDelays)
			data[carrier]['lad'].append(LACDelays)
	
	#sorts through each origin, adds up the weather/security totals, and determines the maximum in the list of origins
	wsDelay = 0
	longestWSDelay = 0
	i = 0
	k = 0
	total = 0
	og = ""
	totalLACD = 0
	for origins in data:
		if 'w+s' in data[origins]:
			for i in data[origins]['w+s']:
				wsDelay += i
			if(wsDelay > longestWSDelay):
				longestWSDelay = wsDelay
				og = origins
			wsDelay = 0
			
		if 'lad' in data[origins]:
			for k in data[origins]['lad']:
				totalLACD += k
				total += 1
			if total != 0:	
				average = totalLACD / total
			print "avg aircraft LAcD: " + origins + " " + str(average) + " minutes"
			average = 0
			totalLACD = 0
			total = 0
	
	
	#sorts through each carrier and determines the largest delay
	carDelay = 0
	longestCDelay = 0
	c = ""
	j = 0
	for carriers in data:
		if 'delay' in data[carriers]:
			for j in data[carriers]['delay']:
				carDelay += j
			if(carDelay > longestCDelay):
				longestCDelay = carDelay
				c = carriers
			carDelay = 0
			
		if 'lad' in data[carriers]:
			for h in data[carriers]['lad']:
				totalLACD += h
				total += 1
			if total != 0:	
				average = totalLACD / total
			print "avg carrier LAcD: " + carriers + " " + str(average) + " minutes"
			average = 0
			totalLACD = 0
			total = 0
	print
	print "longest Weather/Security delays: " + str(longestWSDelay) + " " + og
	print
	print "longest Carrier delays: " + str(longestCDelay) + " " + c
	print
	
	
	
	
	
	
	
	
