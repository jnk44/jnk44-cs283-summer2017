#!/usr/bin/env python
#	Joshua Karmel
#	8/24/17
#	Python 2.7
#	CS 283 - Lab 2 Pt 2

import csv
import sys
import random
from pyspark import SparkContext

sc = SparkContext()

num_samples = 10000000

def inside(p):
	x, y = random.random(), random.random()
	return x*x + y*y < 1

count = sc.parallelize(range(0,num_samples),12).filter(inside).count()
pi = 4 * count/num_samples
print(pi)

sc.stop()
