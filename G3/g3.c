#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

int main(int argc, char** argv[])
{
	const char *PIECES = "XO";

	if(argv[1] != NULL){
		games = atoi(argv[1]);
	}
	else{
		printf("No number of games input.\n");
		exit(1);
	}

	if(argv[2] != NULL){
		size = atoi(argv[2]);
	}
	else{
		printf("No grid size input.\n");
		exit(1);
	}

	int grid[size * size];
	memset(grid, ' ', size*size);

	int turn, done;
	done = 0;

	for(turn = 0; turn < (size*size) && !done; turn++){
		printBoard(grid,size);

		while(!takeTurn(grid, turn%2, PIECES)){
			printBoard(grid);
			printf("**Column full!**\n");
		}
		done = checkWin(grid);
	}
	printBoard(grid,size);

	return 0;
}

//prints the grid
int printBoard(char* grid, int size){
	int i,j;

	for(i = 0; i < size; i++){
		for(j = 0; j <= size; j++){
			if(j == size){
				printf("-");
			}
			else{
				printf("| %c ",grid[size * i + j]);
			}
		}
		printf("|");
		if(j == size){
			printf("Game %d",i+1);
		}
		printf("\n");
	}
}

int takeTurn(char *grid, int player, const char*, int size){
	int row, col = 0;
	printf("Player %d (%c):\nEnter number coordinate: ", player + 1, PIECES[player]);

	while(1){
		if(1 != scanf("%d", &col) || col < 1 || col > 7){
			while(getchar() != '\n');
			printf("Number out of bounds!");
		}
		else{
			break;
		}
	}
	col--;

	for(row = size-1; row >= 0; row--){
		if(grid[size * row + col] == ' '){
			grid[size * row + col] = PIECES[player];
			return 1;
		}
	}
	return 0;
}
