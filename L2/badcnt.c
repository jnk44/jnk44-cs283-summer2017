#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <pthread.h>
#include "csapp.h"

volatile unsigned int cnt = 0;
#define NITERS 1000

int counter = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *thread_func(void *arg){
	int val;
	
	pthread_mutex_lock( &mutex );
	val = counter;
	counter = val + 1;
	pthread_mutex_unlock( &mutex );

	return NULL;
}

void* count(void *arg){
	int i;
	for(i = 0; i < NITERS; i++){
		cnt++;
	}
	return NULL;
}

int main() {
	
	clock_t start = clock(), diff;
	
	pthread_t list[100];
	
	for(int i = 0; i < 100; i++){
		pthread_create(&list[i],NULL,count,NULL);
	}
	
	for(int j = 0; j < 100; j++){
		pthread_join(list[j], NULL);
	}
	
    if (cnt != (unsigned)(NITERS*2)){
        printf("BOOM! cnt=%d\n", cnt);
    }
	else{
        printf("OK cnt=%d\n", cnt);
	}
	
	diff = clock() - start;
	
	int msec = diff * 1000 / CLOCKS_PER_SEC;
	
	printf("Time: %d seconds %d ms\n",msec/1000,msec%1000);
	
	return 0;
}

