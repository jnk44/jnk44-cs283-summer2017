Joshua Karmel
8/18/17
L2

The badcnt.c program has a race condition, because every thread modifies the value of the cnt variable.
__________________________
|run|time (ms)	|output	  |
|=========================|
|1  |13		|cnt=98816|
|2  |13		|cnt=98053|
|3  |12		|cnt=99422|
|4  |14		|cnt=10000|
|5  |13		|cnt=97538|
|6  |17		|cnt=98684|
|7  |21		|cnt=81504|
|8  |12		|cnt=99994|
|9  |13		|cnt=10000|
|10 |12		|cnt=98904|
==========================

badcntlock.c was about 100 ms slower than the other program, but successfully counted.
__________________________
|run|time (ms)	|output	  |
|=========================|
|1  |565	|cnt=10000|
|2  |761	|cnt=10000|
|3  |832	|cnt=10000|
|4  |785	|cnt=10000|
|5  |803	|cnt=10000|
|6  |792	|cnt=10000|
|7  |442	|cnt=10000|
|8  |823	|cnt=10000|
|9  |592	|cnt=10000|
|10 |670	|cnt=10000|
==========================

badcntlock2.c runs much faster, like the original program and counts successfuly.
__________________________
|run|time (ms)	|output	  |
|=========================|
|1  |15		|cnt=10000|
|2  |14		|cnt=10000|
|3  |11 	|cnt=10000|
|4  |15 	|cnt=10000|
|5  |12 	|cnt=10000|
|6  |14 	|cnt=10000|
|7  |11 	|cnt=10000|
|8  |12 	|cnt=10000|
|9  |11 	|cnt=10000|
|10 |12 	|cnt=10000|
==========================

