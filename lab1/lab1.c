#include <stdlib.h>
#include <stdio.h>

typedef struct ListNode{
	int val;
	struct ListNode* next;
}ListNode;

int main(){
	int* a = (int*) malloc(10*sizeof(int));
	int i;
	
	for(i = 0; i < 10; i++){
		*(a+i) = i;
		printf("%d",a[i]);
	}
	printf("\n");
	
	free(a);
	
	char** b = (char**) malloc(10*sizeof(char*));
	int j;
	int k;
	
	for(j = 0; j < 10; j++){
		*(b+j) = (char*) malloc(15*sizeof(char));
		*(b+j) = "procrastinate";
		printf("\n",*(b+j));
	}
	
	free(b);
	
	int* c = (int*) malloc(5*sizeof(int));
	
	*(c) = 5;
	*(c+1) = 4;
	*(c+2) = 3;
	*(c+3) = 2;
	*(c+4) = 1;
	
	sort(c,5);
	
	free(c);
	
	ListNode* head = (ListNode*)malloc(sizeof(ListNode));
	ListNode* x = (ListNode*)malloc(sizeof(ListNode));
	ListNode* y = (ListNode*)malloc(sizeof(ListNode));
	ListNode* z = (ListNode*)malloc(sizeof(ListNode));
	
	head->val = NULL;
	x->val = 3;
	y->val = 2;
	z->val = 1;
	
	head->next = x;
	x->next = y;
	y->next = z;
	
	sort2(head,3);
	
	return 0;
}

void sort(int*a, int size){
	int i;
	int j = 0;
	int end = size-1;
	int tmp;
	
	for(i = 1; i <= end; i++){
		for(j = 0; j <= end-i; j++){
			if(*(a + j) > *(a + j + 1)){
				tmp = *(a+j);
				*(a+j) = *(a+j+1);
				*(a+j+1) = tmp;
			}
		}
		printf("%d",i);
	}
	return;
}

void sort2(ListNode* list, int size){
  ListNode* n = (ListNode*)malloc(sizeof(ListNode));
  n = list;
  ListNode* tmp;
  ListNode* tmp2;
  ListNode* tmp3;
  int i,j;
 
  for(i = 0; i < size-1; i++){
    for(j = 0; j < size-1; j++){
      if(n->next->val > n->next->next->val){
        tmp = n->next->next;
        tmp2 = n->next;
        tmp3 = n->next->next->next;
        n->next = tmp;
        n->next->next = tmp2;
        n->next->next->next = tmp3;
      }
    }
    n = list->next;
  }
  printf("\n");
  for(i = 0; i < size; i++){
    printf("%d",list->next->val);
    list = list->next;
  }
  return;
}
