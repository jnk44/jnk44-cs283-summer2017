#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char** argv[]){

	FILE* file = fopen("poop.txt","w+");
	fclose(file);

	printf("\n");
	char* fileName;
	char* directory = (char*) malloc(sizeof(char)*100);
	strcpy(directory,"./");
	FILE *f;
	
	if(argv[1] != NULL){
		fileName = argv[1];
		printf("Command File: %s\n",fileName);
		f = fopen(fileName,"r");
		if(f < 0){
			perror("Error");
			printf("Invalid File Name!\n");
			exit(1);
		}
	}
	else{
		printf("Please input a command file name\n");
		exit(1);
	}
	
	char buff[255];
	int i = 0;
	
	//gets in each line of the command file
	while((fgets(buff,255,(FILE*)f)) > 0){
		char* line[256];
		char* tok;
		char* commands[32];
	
		printf("Line %d: ",i+1);
		i+=1;
		
		//tokenizes the line into an array, tokens
		strcpy(line,buff);
		tok = strtok(line,"\n");
		tok = strtok(line," ");
		int j = 0;
		while(tok){
			commands[j] = tok;
			printf("%s ",commands[j]); //**tested for tokenization**
			
			tok = strtok(NULL," ");
			j++;
		}
		printf("\n");

		char* command = commands[0];
		if(strcmp(command, "CREATE") == 0){/////////////////////////////////////CREATE
			//printf("%s:\n",command);
			//printf("\nDirectory: %s\n",directory);
			
			if(strcmp(commands[1], "DATABASE") == 0){//CREATE DATABASE
				char* db = (char*) malloc(strlen(commands[2]));
				strcpy(db,commands[2]);
				printf("%s created.\n", db);
				mkdir(db, S_IRWXU | S_IRWXG | S_IRWXO);//Makes new directory for DB
				strcat(db,"/");
				strcat(directory,db);
				//printf("%s\n",commands[2]);
				//printf("\nDirectory: %s\n",directory);
			}
			else if(strcmp(commands[1], "TABLE") == 0){//CREATE TABLE
				char* tableName = commands[2];
				printf("New table name: %s\n",tableName);
				char* filedest = (char*) malloc(strlen(tableName) + strlen("/") + strlen(directory));
				strcpy(filedest,directory);
				strcat(filedest,tableName);
				//printf("\nFile Dest: %s\n",filedest);
				
				char fn[255];
				sprintf(fn,"%s",filedest);
				FILE* tf = fopen(fn,"w+");
				
				if(strcmp(commands[3], "FIELDS") == 0){
					fprintf(tf, commands[4]);
				}
				else{
					printf("TABLE FIELDS INVALID\n");
				}
				
				fclose(tf);
			}
			
			printf("\n");
		}
		else if(strcmp(command, "DROP") == 0){///////////////////////////////////DROP
			if(strcmp(commands[1], "TABLE") == 0){
				char* tableName = commands[2];
				char* filedest = (char*) malloc(strlen(tableName) + strlen("/") + strlen(directory));
				strcpy(filedest,directory);
				strcat(filedest,tableName);
				remove(filedest);
				printf("Table dropped.");
			}
		}
		else if(strcmp(command, "SELECT") == 0){///////////////////////////////////SELECT **INCOMPLETE**
			if(strcmp(commands[1], "DATABASE") == 0){
				char* tableName = commands[2];
				char* filedest = (char*) malloc(strlen(tableName));
				strcpy(filedest,directory);
				strcat(filedest,tableName);
				remove(filedest);
				strcpy(directory,filedest);
			}
			char* table = commands[3];
			char* tf = strcat(directory,table);
			//printf("%s\n",tf);
			char fn[255];
			sprintf(fn,"%s",tf);
			FILE * tblFile = fopen(table,"r");
			
			char* item = (char*) malloc(sizeof(commands[1])+1);
			strcpy(item,commands[1]);
			if(strcmp(command, "*") == 0){
				item = "\*";
			}
			
			char* fields = (char*) malloc(sizeof(commands[5]));
			char* toks;
			char* values[5];
			
			strcpy(fields,commands[5]);
			toks = strtok(fields,"\n");
			toks = strtok(fields,"=");
			
			int k = 0;
			while(toks){
				values[k] = toks;
				//printf("%s\n",values[k]);
				
				toks = strtok(NULL,"\"");
				k++;
			}
			
			char* fFields[255];
			char* fValues[255];
			int index = 0;
			fgets(fFields,255,(FILE*)tblFile);
			printf("%s",fFields);
			
					
		}
	}
		
	
	fclose(f);
	
	printf("\n");
	return 0;
}
/*
char * create(char *commands[], char* dir){
	dir = (char*) malloc(sizeof(char)*5);
	FILE *tf;
	
	if(strcmp(commands[1], "DATABASE") == 0){//creates a database directory
		char* db = commands[2];
		printf("%s Called\n", db);
		mkdir(db, S_IRWXU | S_IRWXG | S_IRWXO);
	}
	else if(strcmp(commands[1], "TABLE") == 0){//creates a table file
		char* tableName = commands[2];
		char* directory = "/table/";
		char* filedest = (char*) malloc(1 + strlen(tableName) + strlen(directory));
		strcpy(filedest,directory);
		strcat(filedest,tableName);
		printf("%s\n",filedest);
		
		char fn[255];
		sprintf(fn,"%s",tableName);
		tf = fopen(fn,"r+");
	}
	
	printf("\n");
	return (dir);
}
*/